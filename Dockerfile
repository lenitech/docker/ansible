FROM python:3.13.0-slim-bullseye@sha256:e28e78623f8f8c3a7f2282a64b74e01a32498048823139015e4ab781175b020e AS build-env
RUN groupadd --gid 1000 ansible \
    && useradd --system --home-dir /home/ansible --create-home --uid 1000 --gid 1000 ansible
COPY --chown=ansible:ansible requirements.txt /home/ansible
COPY --chown=ansible:ansible ansible.cfg /home/ansible/.ansible.cfg
RUN python3 -c "import urllib.request as r; print(r.urlopen('https://apt.releases.hashicorp.com/gpg').read().decode())" > /usr/share/keyrings/hashicorp-archive-keyring.asc
RUN echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.asc] https://apt.releases.hashicorp.com bullseye main" > /etc/apt/sources.list.d/hashicorp.list
RUN apt-get update && apt-get install -y \
  openssh-client \
  sshpass \
  git \
  vault \
  && rm -rf /var/lib/apt/lists/*
USER ansible
ENV PATH=$PATH:/home/ansible/.local/bin
RUN pip install \
        --user \
        --requirement /home/ansible/requirements.txt
